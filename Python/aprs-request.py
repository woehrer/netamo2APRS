import json
import requests

headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) '\
           'AppleWebKit/537.36 (KHTML, like Gecko) '\
           'Chrome/75.0.3770.80 Safari/537.36'}

aprsAPIkey = "XXX" #Hier aprs.fi API Key eingeben
WXStation = "OH2TI"
#https://api.aprs.fi/api/get?name=OH2TI&what=wx&apikey=APIKEY&format=json

urlfront = "https://api.aprs.fi/api/get?name="
urlend = "&what=loc&apikey=" + aprsAPIkey + "&format=json"

url = urlfront + WXStation + urlend

response = requests.get(url,headers=headers,  timeout=60)
if response.status_code == 200:
    rjson = response.json()
    print(rjson)