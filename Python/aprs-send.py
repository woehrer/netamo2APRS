import aprslib

# a valid passcode for the callsign is required in order to send
CALL = "" #hier CALL eingeben
PASSCODE = "" #hier PASSCODE eingeben
AIS = aprslib.IS(CALL, passwd=PASSCODE, port=14580)
AIS.connect()
# send a single status message
AIS.sendall("DL7KC>APRS,TCPIP*:>status text")
